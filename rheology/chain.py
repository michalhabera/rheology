from time import time

from sympy import Derivative, Function, pprint, symbols, solve
from sympy import linear_eq_to_matrix, linsolve, solve_linear_system, Matrix
from sympy import Poly
from sympy.solvers.ode import dsolve, ode_order
from .edge import Edge

class Chain():
    """Chain of rheological components
    
    This class holds nodes of rheological chain. It also provides
    functionality to solve rheological chain and reduce constitutive relations.

    Parameters
    ----------
    nodes : List
        List of nodes contained in this chain.
    """

    def __init__(self, nodes):
        self.nodes = nodes
        self.t = symbols("t")

    def solve(self):
        """Solves this chain

        Reduces every serial and parallel element until trivial
        chain is reached.

        Note
        ----
        This method replaces current chain.
        """

        # Iterate until temporary chain is not trivial
        # Trivial means there are just 2 nodes and only 1 edge
        while not (len(self.nodes) == 2
                   and len(self.nodes[0].edges) == 1):

            midnode = self._find_serial_reducible()
            if midnode is not None:
                self._reduce_serial(midnode)
                continue

            pr = self._find_parallel_reducible()
            if pr is not None:
                self._reduce_parallel(pr[0], pr[1])
                continue

    def _find_serial_reducible(self):
        """Finds reducible serial element in this chain

        Definition
        ----------
        Non-terminal node with exactly two edges

        Returns
        -------
        Middle `Node` of the serial chain or `None` if not found
        """
        for node in self.nodes:
            if not node.terminal and len(node.edges) == 2:
                return node
        return None

    def _find_parallel_reducible(self):
        """Finds reducible parallel element in this chain

        Definition
        ----------
        Two nodes connected directly with at least 2 edges

        Returns
        -------
        List of two nodes or `None` if not found
        """
        for node in self.nodes:
            if len(node.edges) <= 1:
                continue
            connected_nodes = []
            for edge in node.edges:
                connected_node = edge.get_connected_node(node)
                if connected_node in connected_nodes:
                    # If this node was already found
                    return [node, connected_node]
                else:
                    connected_nodes.append(connected_node)
            return None

    def _reduce_serial(self, midnode):
        """Reduces serial element
        
        Parameters
        ----------
        midnode
            Middle node of serial reducible subelement
        """
        edge1 = midnode.edges[0]
        node1 = edge1.get_connected_node(midnode)

        edge2 = midnode.edges[1]
        node2 = edge2.get_connected_node(midnode)

        # Extract local symbols
        t = self.t
        eps1, eps2 = edge1.eps(t), edge2.eps(t)
        sigma1, sigma2 = edge1.sigma(t), edge2.sigma(t)


        # Extract local constitutive relations
        const_rel1 = edge1.eq
        const_rel2 = edge2.eq

        # Prepare new direct edge
        # At this point we connected node1 <---> node2 with new edge
        edge = Edge(self._get_unique_edge_id(), node1, node2)

        # Extract global symbols
        eps, sigma = edge.eps(t), edge.sigma(t)

        # Stress is the only one, sigma = sigma1 = sigma2
        const_rel1 = const_rel1.subs(sigma1, sigma)
        const_rel2 = const_rel2.subs(sigma2, sigma)

        # Strain is additive, eps1 = eps - eps2
        const_rel1 = const_rel1.subs(eps1, eps - eps2).doit().simplify()
        const_rel2 = const_rel2.subs(eps1, eps - eps2).doit().simplify()

        edge.eq = self._eliminate_couple(const_rel1, const_rel2, eps2)

        # At this point serial element is reduced
        # We need to clean up redundant edges and node
        self.nodes.remove(midnode)
        node1.edges.remove(edge1)
        node2.edges.remove(edge2)

    def _reduce_parallel(self, node1, node2):
        """Reduces parallel element
        
        Parameters
        ----------
        node1, node2
            Nodes between which parallel connection is to be reduced.
        """
        # Find two edges that connect these nodes
        edges = []
        for edge in node1.edges:
            if node2 in edge.nodes:
                edges.append(edge)
            if len(edges) == 2:
                break
        edge1, edge2 = edges[0], edges[1]

        # Extract local symbols
        t = self.t
        eps1, eps2 = edge1.eps(t), edge2.eps(t)
        sigma1, sigma2 = edge1.sigma(t), edge2.sigma(t)

        # Extract local constitutive relations
        const_rel1 = edge1.eq
        const_rel2 = edge2.eq

        # Prepare new direct edge
        # At this point we connected node1 <---> node2 with new edge
        edge = Edge(self._get_unique_edge_id(), node1, node2)

        # Extract global symbols
        eps, sigma = edge.eps(t), edge.sigma(t)

        # Stress is the only one, sigma = sigma1 = sigma2
        const_rel1 = const_rel1.subs(eps1, eps)
        const_rel2 = const_rel2.subs(eps2, eps)

        # Strain is additive, eps1 = eps - eps2
        const_rel1 = const_rel1.subs(sigma1, sigma - sigma2).doit().simplify()
        const_rel2 = const_rel2.subs(sigma1, sigma - sigma2).doit().simplify()

        edge.eq = self._eliminate_couple(const_rel1, const_rel2, sigma2)

        # Need to clean up redundant edges
        node1.edges.remove(edge1)
        node1.edges.remove(edge2)

        node2.edges.remove(edge1)
        node2.edges.remove(edge2)

    def _eliminate_couple(self, eq1, eq2, func):
        """Eliminates function from couple of differential equations

        Note
        ----
        Works in following steps:
            1. If any of equation is purely algebraic then it is solved
               and substitued directly
            2. Assembles system matrix by differentiating equations
               until regular system is reached.
            3. Substitutes derivatives to unknowns.
            4. Solves symbolic matrix inverse. Applies inverse to RHS.
            5. Substitutes unknowns back to derivatives.

        Forward and backward substitution for complicated expression is very
        slow in SymPy.
        """

        # Find orders of ODEs
        eq1_order = ode_order(eq1, func)
        eq2_order = ode_order(eq2, func)

        # Determine which equation solve algebraically
        to_solve, to_subs = None, None
        if eq1_order == 0:
            to_solve = eq1
            to_subs = eq2
        elif eq2_order == 0:
            to_solve = eq1
            to_subs = eq2

        if to_solve is not None:
            sol = solve(to_solve, func)[0]
            return to_subs.subs(func, sol).doit()
        else:
            # Both equations are differential here
            eq1_min_order = self._min_order(eq1, func)
            eq2_min_order = self._min_order(eq2, func)

            # Find globaly minimal and maximal order
            minmin = min(eq1_min_order, eq2_min_order)
            maxmax = max(eq1_order, eq2_order)

            # Append equations in order to produce solvable system
            eqns1 = []
            eqns2 = []

            # Include equation with minmin order
            if eq1_min_order > eq2_min_order:
                eqns1.append(eq1)
                to_subs = eq2
            else:
                eqns2.append(eq2)
                to_subs = eq1

            for k in range(1, maxmax - eq1_order + 1):
                eqns1.append(eq1.diff(self.t, k))

            for k in range(1, maxmax - eq2_order + 1):
                eqns2.append(eq2.diff(self.t, k))

            # At this point no new variable was generated
            # If number of unknowns is the same as eqns we are done
            dim_kern = (maxmax - minmin + 1) - (len(eqns1) + len(eqns2))
            if dim_kern == 0:
                raise RuntimeError("Solve system and return")

            # At this point the only way to fill up the system is
            # by generating new unknowns

            for k in range(1, dim_kern + 1):
                if len(eqns1) > 0:
                    eqns1.append(eqns1[-1].diff(self.t))
                else:
                    eqns1.append(eq1.diff(self.t))
                if len(eqns2) > 0:
                    eqns2.append(eqns2[-1].diff(self.t))
                else:
                    eqns2.append(eq2.diff(self.t))

            alg_eqns = []
            for eqn in eqns1 + eqns2:
                alg_eqns.append(self._replace_diffs(eqn, func).expand())

            xs = symbols("x{}:{}".format(minmin, len(alg_eqns)), cls=Function)
            A, b = linear_eq_to_matrix(alg_eqns, [x(self.t) for x in xs])

            b.col_insert(0, A)
            # LU solver is a bit slower, but gives "nicer" results
            sol = A.inv("LU") * b

            # FIXME: Substitution could get very expensive
            for k, x in enumerate(xs):
                to_subs = self._replace_diffs(to_subs, func).subs(x(self.t), sol[k])
            return to_subs

    def _replace_diffs(self, eq, func):
        """Replace derivatives with unknowns
        """
        order = ode_order(eq, func)
        xs = symbols("x0:{}".format(order + 1), cls=Function)

        for k in range(len(xs)):
            if k == 0:
                eq = eq.subs(func, xs[0](self.t))
            else:
                eq = eq.subs(xs[k-1](self.t).diff(), xs[k](self.t))

        return eq

    def _min_order(self, expr, func):
        """Finds minimal derivative order in `expr`

        Note
        ----
        This traverses expression tree recursively.
        Consider when speed is a concern.
        """

        if isinstance(expr, Derivative):
            if expr.args[0] == func:
                return len(expr.variables)
            else:
                order = 9999
                for arg in expr.args[0].args:
                    order = min(order, ode_order(arg, func) + len(expr.variables))
                return order if order != 9999 else 0
        else:
            order = 9999
            for arg in expr.args:
                order = min(order, ode_order(arg, func))
            return order if order != 9999 else 0

    def _get_unique_edge_id(self):
        """Returns unique edge ID across edges in this chain
        """
        ids = []
        for node in self.nodes:
            for edge in node.edges:
                ids.append(edge.id)
        return max(ids) + 1
