class Node():
    def __init__(self, id, terminal=False):
        self.id = id
        self.edges = list()
        self.terminal = terminal

    def __hash__(self):
        return hash(self.id)
