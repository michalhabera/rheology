from sympy import Function, symbols

class Edge():
    """Edge connecting exactly 2 nodes
    """

    def __init__(self, id, node1, node2):
        self.id = id
        self.nodes = [node1, node2]
        self.eps = Function("varepsilon" + str(id))
        self.sigma = Function("sigma" + str(id))
        self.eq = None

        node1.edges.append(self)
        node2.edges.append(self)

    def __hash__(self):
        return hash(self.id)

    def get_connected_node(self, node1):
        """Returns the connected node

        For node passed as parameter return node that is
        connected by this edge

        Parameters
        ----------
        node1
        """
        if node1 not in self.nodes:
            raise RuntimeError("Edge is not connecting the node")
        for node in self.nodes:
            if node is not node1:
                return node
        raise RuntimeError("No other connected node found")