from setuptools import setup

setup(name='rheology',
      version='0.1',
      description='Symbolic 1D rheological chain solver',
      url='https://bitbucket.org/michalhabera/rheology',
      author='Michal Habera',
      author_email='michal.habera@gmail.com',
      license='MIT',
      packages=['rheology'],
      zip_safe=False)