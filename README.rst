.. image:: https://circleci.com/bb/michalhabera/fenicssm/tree/master.svg?style=svg
    :target: https://circleci.com/bb/michalhabera/fenicssm/tree/master
    
========
Rheology
========

Python package for one-dimensional rheology models. Allows creating rheology chain with
arbitrary linear constitutive relations.

-----

Prerequisities
==============
- ``sympy``. Install with ``pip3 install sympy``.
   
-----  

Installing
==========
In package root directory run

.. code:: bash

    pip3 install .

-----

Run tests
=========

To verify that your installation of this package was done succesfully run tests

.. code:: bash

    python3 -m pytest tests/
    
-----

Example usage
=============

See e.g. ``tests/test_basic_elements.py``
    