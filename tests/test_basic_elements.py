"""Simple const. relation tests
"""

from sympy import Function, latex, pprint, symbols, solve, simplify
from sympy import exp, Integral, Derivative, dsolve, Poly, Monomial, collect

from rheology import Edge, Chain, Node

def test_maxwell():
    start_node = Node(0, terminal=True)
    end_node = Node(2, terminal=True)
    mid_node = Node(1, terminal=False)

    maxw_chain = Chain([start_node, mid_node, end_node])
    t = maxw_chain.t
    E, mu = symbols("E mu")

    spring = Edge(0, start_node, mid_node)
    spring.eq = spring.sigma(t) - E * spring.eps(t)

    dashpot = Edge(1, mid_node, end_node)
    dashpot.eq = dashpot.sigma(t) - mu * dashpot.eps(t).diff(t)

    maxw_chain.solve()
    single_edge = maxw_chain.nodes[0].edges[0]
    eps, sigma = single_edge.eps(t), single_edge.sigma(t)
    maxwell = -mu * (eps.diff() - sigma.diff() / E) + sigma
    assert (maxwell == single_edge.eq)


def test_kelvin_voigt():
    start_node = Node(0, terminal=True)
    end_node = Node(1, terminal=True)

    kelv_chain = Chain([start_node, end_node])
    t = kelv_chain.t
    E, mu = symbols("E mu")

    spring = Edge(0, start_node, end_node)
    spring.eq = spring.sigma(t) - E * spring.eps(t)

    dashpot = Edge(1, start_node, end_node)
    dashpot.eq = dashpot.sigma(t) - mu * dashpot.eps(t).diff(t)

    kelv_chain.solve()
    single_edge = kelv_chain.nodes[0].edges[0]
    eps, sigma = single_edge.eps(t), single_edge.sigma(t)
    kelvin = -E * eps - mu * eps.diff() + sigma

    assert kelvin == single_edge.eq

def test_burgers():
    n1 = Node(0, terminal=True)
    n2 = Node(1)
    n3 = Node(2)
    n4 = Node(3, terminal=True)

    E1, mu1, E2, mu2 = symbols("E1, mu1, E2, mu2")

    burg_chain = Chain([n1, n2, n3, n4])
    t = burg_chain.t

    # Spring in kelvin part
    s1 = Edge(0, n1, n2)
    s1.eq = s1.sigma(t) - E1 * s1.eps(t)
    # Dashpot in kelvin part
    d1 = Edge(1, n1, n2)
    d1.eq = d1.sigma(t) - mu1 * d1.eps(t).diff()

    # Spring in maxwell part
    e2 = Edge(2, n2, n3)
    e2.eq = e2.sigma(t) - E2 * e2.eps(t)
    # Dashpot in maxwell part
    d2 = Edge(3, n3, n4)
    d2.eq = d2.sigma(t) - mu2 * d2.eps(t).diff()

    burg_chain.solve()
    single_edge = burg_chain.nodes[0].edges[0]
    eps, sigma = single_edge.eps(t), single_edge.sigma(t)
    burgers = (sigma 
               + (mu1 / E1 + mu2 / E1 + mu2 / E2) * sigma.diff() 
               + mu1 * mu2 / (E1 * E2) * sigma.diff(t, 2) 
               - mu2 * eps.diff() 
               - mu1 * mu2 / E1 * eps.diff(t, 2))

    assert (solve(burgers, sigma)[0] 
            - solve(single_edge.eq, sigma)[0] == 0)

# Author: Martin Rehor
def test_burgers2():
    start_node = Node(0, terminal=True)
    mid_node_1 = Node(1, terminal=False)
    mid_node_2 = Node(2, terminal=False)
    end_node = Node(3, terminal=True)

    burg_chain = Chain([start_node, mid_node_1, mid_node_2, end_node])
    t = burg_chain.t
    E1, E2, mu1, mu2 = symbols("E1 E2 mu1 mu2")

    spring_1 = Edge(0, start_node, mid_node_1)
    spring_1.eq = spring_1.sigma(t) - E1 * spring_1.eps(t)

    dashpot_1 = Edge(1, mid_node_1, end_node)
    dashpot_1.eq = (dashpot_1.sigma(t)
        - mu1 * dashpot_1.eps(t).diff(t))

    spring_2 = Edge(2, start_node, mid_node_2)
    spring_2.eq = spring_2.sigma(t) - E2 * spring_2.eps(t)

    dashpot_2 = Edge(3, mid_node_2, end_node)
    dashpot_2.eq = (dashpot_2.sigma(t)
        - mu2 * dashpot_2.eps(t).diff(t))

    burg_chain.solve()
    single_edge = burg_chain.nodes[0].edges[0]
    eps, sigma = single_edge.eps(t), single_edge.sigma(t)

    burgers2 = (sigma 
                + (mu1 / E1 + mu2 / E2) * sigma.diff() 
                + mu1 * mu2 / (E1 * E2) * sigma.diff(t, 2) 
                - (mu1 + mu2) * eps.diff() 
                - (mu1 * mu2 / E1 + mu1 * mu2 / E2) * eps.diff(t, 2))

    assert (solve(burgers2, sigma)[0]
            - solve(single_edge.eq, sigma)[0].simplify() == 0)


def test_burgers3():
    start_node = Node(0, terminal=True)
    mid_node_1 = Node(1, terminal=False)
    mid_node_2 = Node(2, terminal=False)
    end_node = Node(3, terminal=True)

    burg_chain = Chain([start_node, mid_node_1, mid_node_2, end_node])
    t = burg_chain.t
    E1, E2, mu1, mu2, mu3 = symbols("E1 E2 mu1 mu2 mu3")

    spring_1 = Edge(0, start_node, mid_node_1)
    spring_1.eq = spring_1.sigma(t) - E1 * spring_1.eps(t)

    dashpot_1 = Edge(1, mid_node_1, end_node)
    dashpot_1.eq = dashpot_1.sigma(t) - mu1 * dashpot_1.eps(t).diff(t)

    spring_2 = Edge(2, start_node, mid_node_2)
    spring_2.eq = spring_2.sigma(t) - E2 * spring_2.eps(t)

    dashpot_2 = Edge(3, mid_node_2, end_node)
    dashpot_2.eq = dashpot_2.sigma(t) - mu2 * dashpot_2.eps(t).diff(t)

    dashpot_3 = Edge(4, start_node, end_node)
    dashpot_3.eq = dashpot_3.sigma(t) - mu3 * dashpot_3.eps(t).diff(t)

    burg_chain.solve()
    single_edge = burg_chain.nodes[0].edges[0]
    eps, sigma = single_edge.eps(t), single_edge.sigma(t)


def test_wild_parallel():
    N = 4
    start_node = Node(0, terminal=True)
    end_node = Node(1, terminal=True)

    kelv_chain = Chain([start_node, end_node])
    t = kelv_chain.t
    Es = symbols("E0:{}".format(N))
    mus = symbols("mu0:{}".format(N))

    for k in range(N):
        maxw = Edge(k, start_node, end_node)
        maxw.eq = -mus[k] * (maxw.eps(t).diff() - maxw.sigma(t).diff() / Es[k]) + maxw.sigma(t)

    kelv_chain.solve()
    single_edge = kelv_chain.nodes[0].edges[0]
    eps, sigma = single_edge.eps(t), single_edge.sigma(t)
